use std::{ffi::OsString, path::{Path, PathBuf}};

fn get_files(path: &Path, extension: &str) -> Vec<PathBuf> {
    let entries = std::fs::read_dir(path)
        .unwrap()
        .filter_map(|f| {
            let funw = f.unwrap();
            match funw.path().extension() {
                Some(ext) => if ext.eq(&OsString::from(extension)) {
                    return Some(funw.path());
                } else {
                    return None;
                },
                _ => None
            }
        }).collect::<Vec<_>>();
    entries
}

fn main() {
/*
    let asmentries = get_files(&Path::new("src/ffi/asm"), "s");

    cc::Build::new()
        .no_default_flags(true)
        .static_flag(true)
        .files(&asmentries)
        .flag("-ffreestanding")
        .flag("-lgcc")
        .flag("-nostdlib")
        .compile("vesadrv")
*/
}