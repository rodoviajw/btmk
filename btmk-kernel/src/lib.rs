#![no_std]
#![feature(abi_x86_interrupt)]
#![feature(const_fn_trait_bound)]
#![feature(alloc_error_handler)]
#![warn(noop_method_call)]


pub use x86_64;

extern crate alloc;

pub mod allocate;
pub mod interrupts;
pub mod vga;
pub mod volatile;
pub mod gdt;
pub mod memory;
//pub mod ffi;

pub fn init() {
    gdt::create();
    interrupts::create();
    unsafe { interrupts::PICS.lock().initialize() };
    x86_64::instructions::interrupts::enable();
}

#[alloc_error_handler]
fn alloc_error_handler(layout: alloc::alloc::Layout) -> ! {
    panic!("allocation error: {:?}", layout)
}

pub fn halt() -> ! {
    loop {
        x86_64::instructions::hlt();
    }
}