use pic8259::ChainedPics;
use spin::Mutex;
use x86_64::{structures::idt::{InterruptDescriptorTable, InterruptStackFrame, PageFaultErrorCode}};
use crate::{print, println, halt};
use lazy_static::lazy_static;

pub const PIC1_OFFSET: u8 = 32;
pub const PIC2_OFFSET: u8 = PIC1_OFFSET + 8;

pub static PICS: Mutex<ChainedPics> = Mutex::new(
    unsafe { ChainedPics::new(PIC1_OFFSET, PIC2_OFFSET) }
);

#[derive(Clone, Copy, Debug)]
#[repr(u8)]
pub enum InterruptIndex {
    Timer = PIC1_OFFSET,
    Keyboard
}

impl InterruptIndex {
    pub fn as_u8(self) -> u8 {
        self as u8
    }

    pub fn as_usize(self) -> usize {
        usize::from(self.as_u8())
    }
}

lazy_static! {
    static ref IDT: InterruptDescriptorTable = {
        let mut idt = InterruptDescriptorTable::new();
        idt.breakpoint.set_handler_fn(breakpoint_handler);
        idt.page_fault.set_handler_fn(page_fault_handler);
        //idt.double_fault.set_handler_fn(doublebreakpoint_handler);
        unsafe {
            idt.double_fault.set_handler_fn(doublebreakpoint_handler)
                .set_stack_index(crate::gdt::DOUBLE_FAULT_IST_INDEX);
        }
        idt[InterruptIndex::Timer.as_usize()]
            .set_handler_fn(timerbreakpoint_handler);
        idt[InterruptIndex::Keyboard.as_usize()]
            .set_handler_fn(keyboardinterrupt_handler);
            
        idt
    };
}

pub fn create() {
    IDT.load();
}

extern "x86-interrupt"
fn page_fault_handler(
    stack_frame: InterruptStackFrame, 
    error_code: PageFaultErrorCode
) {
    use x86_64::registers::control::Cr2;
    
    println!("PAGE FAULT");
    println!("Accessed Address: {:?}", Cr2::read());
    println!("Error with code {:#?}", error_code);
    println!("Stack: {:#?}", stack_frame);
    halt();
}

extern "x86-interrupt" 
fn breakpoint_handler(stack: InterruptStackFrame) {
    println!("BREAKPOINT: {:#?}", stack);
}

extern "x86-interrupt"
fn doublebreakpoint_handler(stack: InterruptStackFrame, _error_code: u64) -> ! {
    panic!("DOUBLE FAULT:\n {:#?}", stack);
}

extern "x86-interrupt"
fn timerbreakpoint_handler(_stack: InterruptStackFrame) {
    print!(".");

    unsafe {
        PICS.lock()
            .notify_end_of_interrupt(InterruptIndex::Timer.as_u8());
    }
}

extern "x86-interrupt"
fn keyboardinterrupt_handler(_stack: InterruptStackFrame) {
    use pc_keyboard::{layouts, DecodedKey, HandleControl, Keyboard, ScancodeSet1};
    use x86_64::instructions::port::Port;

    lazy_static! {
        static ref KEYBOARD: Mutex<Keyboard<layouts::Us104Key, ScancodeSet1>> =
            Mutex::new(Keyboard::new(layouts::Us104Key, ScancodeSet1,
                HandleControl::Ignore)
            );
    }

    let mut keyboard = KEYBOARD.lock();
    let mut port = Port::new(0x60);
    let scancode: u8 = unsafe { port.read() };

    if let Ok(Some(key_event)) = keyboard.add_byte(scancode) {
        if let Some(key) = keyboard.process_keyevent(key_event) {
            match key {
                DecodedKey::Unicode(character) => print!("{}", character),
                DecodedKey::RawKey(key) => print!("{:?}", key),
            }
        }
    }
    

    unsafe {
        PICS.lock()
            .notify_end_of_interrupt(InterruptIndex::Keyboard.as_u8());
    }
}
