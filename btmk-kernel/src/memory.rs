use core::slice::Iter;

use bootloader::{boot_info::{MemoryRegion, MemoryRegionKind as MemoryRegionType}};
use x86_64::{PhysAddr, VirtAddr, registers::control::Cr3, structures::paging::{FrameAllocator, OffsetPageTable, PageTable, PhysFrame, Size4KiB, page_table::FrameError}};

#[repr(C)]
pub struct MemoryMap {
    entries: [MemoryRegion; 64],
    next_entry_index: u64
}

impl MemoryMap {
    pub fn new() -> Self {
        MemoryMap {
            entries: [MemoryRegion::empty(); 64],
            next_entry_index: 0,
        }
    }

    pub fn add_region(&mut self, region: MemoryRegion) {
        assert!(
            self.next_entry_index() < 64,
            "Too many memory region maps"
        );

        self.entries[self.next_entry_index()] = region;
        self.next_entry_index += 1;
    }

    pub fn next_entry_index(&self) -> usize {
        self.next_entry_index as usize        
    }

    pub fn iter(&self) -> impl Iterator<Item = &MemoryRegion> {
        self.entries.iter()
    }
}

/// Initialize a new OffsetPageTable.
///
/// This function is unsafe because the caller must guarantee that the
/// complete physical memory is mapped to virtual memory at the passed
/// `physical_memory_offset`. Also, this function must be only called once
/// to avoid aliasing `&mut` references (which is undefined behavior).
pub unsafe fn init_op_table(physical_memory_offset: VirtAddr) 
    -> OffsetPageTable<'static>
{
    let l4_table = active_l4_page_table(physical_memory_offset);
    OffsetPageTable::new(l4_table, physical_memory_offset)
}

pub struct BootInfoFrameAllocator {
    memory_map: &'static MemoryMap,
    next: usize
}

impl BootInfoFrameAllocator {
    pub unsafe fn init(memory_map: &'static MemoryMap) -> Self {
        Self {
            memory_map,
            next: 0
        }
    }

    fn usable_frames(&self) -> impl Iterator<Item = PhysFrame> {
        let regions = self.memory_map.iter();
        let usable_regions = regions.filter(
            |r| r.kind == MemoryRegionType::Usable
        );
        
        let addr_ranges = usable_regions.map(
            |r| r.start..r.end
        );
        let frame_addresses = addr_ranges.flat_map(|r| r.step_by(4096));
        frame_addresses.map(|addr| PhysFrame::containing_address(PhysAddr::new(addr)))
    }
}

unsafe impl FrameAllocator<Size4KiB> for BootInfoFrameAllocator {
    fn allocate_frame(&mut self) -> Option<PhysFrame<Size4KiB>> {
        let frame = self.usable_frames().nth(self.next);
        self.next += 1;
        frame
    }
}

/// Returns a mutable reference to the active level 4 table.
///
/// This function is unsafe because the caller must guarantee that the
/// complete physical memory is mapped to virtual memory at the passed
/// `physical_memory_offset`. Also, this function must be only called once
/// to avoid aliasing `&mut` references (which is undefined behavior).
unsafe fn active_l4_page_table(physical_memory_offset: VirtAddr)
    -> &'static mut PageTable
{

    let (l4_frame, _) = Cr3::read();
    let phys = l4_frame.start_address();
    let virt = physical_memory_offset + phys.as_u64();
    let page_tableptr: *mut PageTable = virt.as_mut_ptr();

    &mut *page_tableptr
}

/// Translates virtual address *addr* to physical. Or `None` if not mapped.
/// 
/// This function is unsafe because the caller must guarantee that the
/// complete physical memory is mapped to virtual memory at the passed
/// `physical_memory_offset`.
pub unsafe fn translate_addr(addr: VirtAddr, physical_memory_offset: VirtAddr)
    -> Option<PhysAddr>
{
    translate_addr_inner(addr, physical_memory_offset)
}

// The [translate_addr] implementation.
fn translate_addr_inner(addr: VirtAddr, physical_memory_offset: VirtAddr)
    -> Option<PhysAddr> 
{
    let (l4_table, _) = Cr3::read();
    let table_indexes = [
        addr.p4_index(), addr.p3_index(), addr.p2_index(), addr.p1_index()
    ];

    let mut frame = l4_table;

    for &index in &table_indexes {
        let virt = physical_memory_offset + frame.start_address().as_u64();
        let table_ptr: *const PageTable = virt.as_ptr();
        let table = unsafe {&*table_ptr};

        let entry = &table[index];
        frame = match entry.frame() {
            Ok(frame) => frame,
            Err(FrameError::FrameNotPresent) => return None,
            Err(FrameError::HugeFrame) => panic!("huge pages not supported"),
        };
    }

    Some(frame.start_address() + u64::from(addr.page_offset()))
}

#[inline(always)]
pub(crate) fn realfptr(i: u16, e: u16) -> u16 {
    (i + 16 * e) as u16
}
