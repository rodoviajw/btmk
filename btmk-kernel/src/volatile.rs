//! Easy bypass of compiler optimizations. 
//!
//! Volatile memory is never optimized by the compiler, making it useful for bare metal contexts.
//! This is a alternative implementation of [volatile]. Since the crate only supports smart pointers 
//! (i.e structs that implement [Deref])
//!
//! [volatile]: volatile::Volatile
//! [Deref]: core::ops::Deref

use core::ptr;

/// Represents a volatile [Copy]able type.
/// See the module docs for more information.
///
/// [Copy]: core::marker::Copy
pub struct Volatile<T: Copy>(T);

impl<T: Copy> Volatile<T> {
    /// Creates a new volatile type
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use btmk_kernel::volatile::Volatile;
    ///
    /// # fn main() {
    /// let mut data = 32 as usize;
    /// let volt = Volatile::new(data);
    /// ```
    pub const fn new(data: T) -> Self {
        Volatile(data)
    }

    /// Returns the current internal data.
    /// 
    /// The data might be set in the [constructor] or at [write] method.
    /// 
    /// # Examples
    /// 
    /// ```rust
    /// // for context on what is `volt`, see the constructor example!
    /// let read = volt.read();
    /// assert_eq!(read, 32 as usize);
    /// ```
    pub fn read(&self) -> T {
        unsafe { ptr::read_volatile(&self.0) }
    }

    /// Ovewrites the current data.
    ///
    /// The overwrite is abrupt, this means that the current data will be replaced without
    /// running it's destructor. To avoid memory leaks, call [drop] when the object needs to be
    /// dropped.
    /// 
    /// # Examples
    ///
    /// ```rust
    /// // for context on what is `volt`, see the constructor example!
    /// volt.write(31 as usize);
    /// assert_eq!(volt.read(), 31 as usize);
    /// ```
    pub fn write(&mut self, data: T) {
        unsafe { ptr::write_volatile(&mut self.0, data) }
    }
}

impl<T: Copy> Clone for Volatile<T> {
    fn clone(&self) -> Self {
        Volatile(self.read())
    }
}