#include "vga.h"

char *strncpy(
   char *strDest,
   const char *strSource,
   long unsigned int count
) {
   int i;
   char *temp;
   temp = strDest;  
   for (i = 0; i < count; i++)
      *strDest++ = *strSource++;
   return temp;
}

uint16_t VbeFindMode(int x, int y, int d) {
    struct VbeInfoBlock *ctrl = (struct VbeInfoBlock *) 0x2000;
    struct VbeModeInfo *modeinf = (struct VbeModeInfo *) 0x3000;
    uint16_t *modes;
    int i;
	uint16_t status;
    uint16_t best = 0x13;
    int pixdiff, bestpixdiff = DIFF(300 * 200, x * y);
    int depthdiff, bestdepthdiff = 8 >= d ? 8 - d : (d - 8) * 2;

    strncpy(ctrl->VbeSignature, "VBE2", 5);
	status = VbeGetControllerInfo(ctrl);
	if (status != 0x004F) return best;

	modes = (uint16_t) ctrl->VideoModePtr;
	for (i = 0; modes[i] != 0xFFFF; i++) {
		status = VbeGetModeInfo(modes[i], modeinf);
		if (status != 0x004F) continue;

		// Has linear framebuffer?
		if ((modeinf->attributes & 0x90) != 0x90) continue;

		// Is a packed pixel or direct color mode?
		if (modeinf->memory_model != 4 && modeinf->memory_model != 6) continue;

		// Is this the _exact_ mode we are looking for?
		if (x == modeinf->width 
			&& y == modeinf->height 
			&& d == modeinf->bpp) {
			return modes[i];
		}
		pixdiff = DIFF(modeinf->width * modeinf->height, x * y);
		depthdiff = (modeinf->bpp >= d)? modeinf->bpp - d : (d - modeinf->bpp) * 2;
		if ( bestpixdiff > pixdiff ||
			(bestpixdiff == pixdiff && bestdepthdiff > depthdiff) ) {
			best = modes[i];
			bestpixdiff = pixdiff;
			bestdepthdiff = depthdiff;
		}
		
	}

	if ( x == 640 && y == 480 && d == 1 ) return 0x11;
	return best;
} 
