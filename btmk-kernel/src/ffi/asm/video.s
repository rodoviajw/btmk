.text
.code16

.global VbeSetVideoMode
VbeSetVideoMode:
    mov $0x00, %ah
    mov rsi, %bx
    int $0x10

    mov %al, rax
    ret

.global VbeGetControllerInfo
VbeGetControllerInfo:
    mov rsi, %di
    mov $0x4F00, %ax
    int $0x10
    mov %al, rax
    ret

.global VbeGetModeInfo
VbeGetModeInfo:
    mov $0x4F01, %ax
    mov rsi, %cx
    mov rdx, %di

    int $0x10
    mov %al, rax
    ret
