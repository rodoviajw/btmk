macro_rules! __basic_deft {
    ($st:ident) => {
        impl Default for $st {
            fn default() -> Self {
                unsafe {
                    core::mem::zeroed()
                }
            }
        }
    }
}

#[repr(C, packed)]
#[allow(non_snake_case)]
pub struct VbeInfoBlock {
    pub VbeSignature: [u8; 4usize],
    pub VbeVersion: u16,
    pub OemStringPtr: [u16; 2usize],
    pub Capabilities: [u8; 4usize],
    pub VideoModePtr: [u16; 2usize],
    pub TotalMemory: u16,
}

#[repr(C, packed)]
pub struct VbeModeInfo {
    pub attributes: u16,
    pub window_a: u8,
    pub window_b: u8,
    pub granularity: u16,
    pub window_size: u16,
    pub segment_a: u16,
    pub segment_b: u16,
    pub win_func_ptr: u32,
    pub pitch: u16,
    pub width: u16,
    pub height: u16,
    pub w_char: u8,
    pub y_char: u8,
    pub planes: u8,
    pub bpp: u8,
    pub banks: u8,
    pub memory_model: u8,
    pub bank_size: u8,
    pub image_pages: u8,
    pub reserved0: u8,
    pub red_mask: u8,
    pub red_position: u8,
    pub green_mask: u8,
    pub green_position: u8,
    pub blue_mask: u8,
    pub blue_position: u8,
    pub reserved_mask: u8,
    pub reserved_position: u8,
    pub direct_color_attributes: u8,
    pub framebuffer: u32,
    pub off_screen_mem_off: u32,
    pub off_screen_mem_size: u16,
    pub reserved1: [u8; 206usize],
}

__basic_deft!(VbeModeInfo);
__basic_deft!(VbeInfoBlock);

// Functions defined in vga.h
#[link(name = "vesadrv", kind = "static")]
#[allow(dead_code)]
#[allow(non_snake_case)]
extern "sysv64" {
    pub(crate) fn VbeSetVideoMode(video_mode: u16) -> u16;
    pub(crate) fn VbeGetControllerInfo(vbeMinfo: *mut VbeInfoBlock) -> u16;
    pub(crate) fn VbeGetModeInfo(mode: u16, vbe: *mut VbeModeInfo);
}
