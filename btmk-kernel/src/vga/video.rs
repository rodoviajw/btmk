use crate::ffi::{self, VbeInfoBlock};
use crate::ffi::VbeModeInfo;
use crate::memory::realfptr;
use alloc::format;
use cstr_core::CString;

fn rawstr0<const T: usize>(string: &str) -> [u8; T] {
    CString::new(string).unwrap().as_bytes().try_into()
        .expect(format!("btmk_kernel::vga::video::rawstr0: array size overflow (expected {})", T).as_str())
}

pub fn find_mode(x: i32, y: i32, density: i32) -> u16 {
    let mode = find_mode0(x, y, density);
    mode
}

pub fn set_mode(mode: u16) {
    unsafe {
        crate::ffi::VbeSetVideoMode(mode);
    }
}

pub fn get_mode_info(mode: u16) -> ffi::VbeModeInfo {
    let mut modeinfo = VbeModeInfo::default();
    unsafe { ffi::VbeGetModeInfo(mode, &mut modeinfo as *mut _) };
    modeinfo
}

#[export_name = "VbeFindVideoMode"]
pub fn find_mode0(x: i32, y: i32, density: i32) -> u16 {
    let d = density;
    let mut ctrl = VbeInfoBlock::default();
    ctrl.VbeSignature = rawstr0::<4>("VBE2");

    let status = unsafe { crate::ffi::VbeGetControllerInfo(&mut ctrl as *mut _) };
    if status != 0x004F {
        return 0x13;
    }

    let pixdiff = (300 * 200) - (x * y);
    let depthdiff = if 8 >= density { 8 - d } else { (d - 8) * 2 };

    let modes = ctrl.VideoModePtr;
    let rpt = realfptr(modes[0], modes[1]);
    let slice = unsafe { alloc::slice::from_raw_parts(&rpt as *const u16, 12) };

    for mode in slice {
        let modeinf = get_mode_info(mode.clone());
        if i32::from(modeinf.height) == y &&
            i32::from(modeinf.width) == x 
        {
            return mode.clone();
        }
    }
    return 0x13;
}

