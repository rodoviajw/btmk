#![no_std]
#![no_main]

extern crate alloc;

use bootloader::{BootInfo, entry_point};
use btmk_kernel::{println, x86_64::VirtAddr};
use core::panic::PanicInfo;
use alloc::boxed::Box;
use btmk_kernel::vga::video;

use btmk_kernel::memory::BootInfoFrameAllocator;
use btmk_kernel::memory;

pub const VIDEO_HEIGHT: i32 = 480;
pub const VIDEO_WIDTH: i32 = 640;
pub const VIDEO_DENSITY: i32 = 1;

entry_point!(btmain);

pub fn btmain(bootinfo: &'static BootInfo) -> ! {
    btmk_kernel::init();

    let pmemoff = VirtAddr::new(bootinfo.physical_memory_offset);
    let mut mapper = unsafe { memory::init_op_table(pmemoff) };
    let mut frame_alloc = unsafe {
        BootInfoFrameAllocator::init(&bootinfo.memory_map)
    };

    btmk_kernel::allocate::init_heap(&mut mapper, &mut frame_alloc)
        .expect("Cannot start heap allocation.");

    btmk_kernel::halt();
}

#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    println!("{}", info);
    btmk_kernel::halt();
}
